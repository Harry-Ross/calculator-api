﻿namespace CalculatorAPI.Services
{
    public class ArithmeticService : IArithmeticService
    {
        private readonly ILogger _logger;
        public ArithmeticService(ILogger<ArithmeticService> logger)
        {
            _logger = logger;
        }

        public double Add(double a, double b)
        {
            _logger.LogInformation("Adding {0} and {1}", a, b);
            return a + b;
        }

        public double Subtract(double a, double b)
        {
            _logger.LogInformation("Subtracting {0} and {1}", a, b);
            return a - b;
        }

        public double Multiply(double a, double b)
        {
            _logger.LogInformation("Multiplying {0} and {1}", a, b);
            return a * b;
        }

        public double Divide(double a, double b)
        {
            _logger.LogInformation("Dividing {0} and {1}", a, b);
            return a / b;
        }
    }
}
