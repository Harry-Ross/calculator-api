﻿using CalculatorAPI.Models;
using CalculatorAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace CalculatorAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class ArithmeticController : Controller
    {
        private IArithmeticService _arithmeticService;
        public ArithmeticController(IArithmeticService arithmeticService) 
        { 
            _arithmeticService = arithmeticService;
        }

        [HttpPost]

        public ActionResult<ArithmeticResult> Add([FromBody] ArithmeticRequest req)
        {
            var resultNum = _arithmeticService.Add(req.num1, req.num2);
            var resultObj = new ArithmeticResult(req.num1, req.num2, resultNum);
            return resultObj;
        }

        [HttpPost]
        public ActionResult<ArithmeticResult> Subtract([FromBody] ArithmeticRequest req)
        {
            var resultNum = _arithmeticService.Subtract(req.num1, req.num2);
            var resultObj = new ArithmeticResult(req.num1, req.num2, resultNum);
            return resultObj;
        }

        [HttpPost]
        public ActionResult<ArithmeticResult> Multiply([FromBody] ArithmeticRequest req)
        {
            var resultNum = _arithmeticService.Multiply(req.num1, req.num2);
            var resultObj = new ArithmeticResult(req.num1, req.num2, resultNum);
            return resultObj;
        }

        [HttpPost]
        public ActionResult<ArithmeticResult> Divide([FromBody] ArithmeticRequest req)
        {
            var resultNum = _arithmeticService.Divide(req.num1, req.num2);
            var resultObj = new ArithmeticResult(req.num1, req.num2, resultNum);
            return resultObj;
        }
    }
}
