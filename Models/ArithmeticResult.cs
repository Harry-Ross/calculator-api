﻿namespace CalculatorAPI.Models
{
    public class ArithmeticResult
    {
        public ArithmeticResult(double num1, double num2, double result) {
            this.result = result;
            this.num1 = num1;
            this.num2 = num2;
        }

        public double num1 { get; set; }
        public double num2 { get; set; }
        public double result { get; set; }
    }
}
