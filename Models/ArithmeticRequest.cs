﻿namespace CalculatorAPI.Models
{
    public class ArithmeticRequest
    {
        public double num1 { get; set; }
        public double num2 { get; set; }
    }
}
